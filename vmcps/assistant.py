#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``assistant`` module of ``vmcps`` package."""

from abc import (
    ABCMeta,
    abstractmethod
)
from typing import (
    TypeVar,
    Any,
    Literal
)
from types import TracebackType
# OSC protocol layer
from vmcp.osc import OSC
from vmcp.osc.channel import Sender
# VMC protocol layer
from vmcp.typing import Timestamp

AssistantClass = TypeVar("AssistantClass", bound="Assistant")


class Assistant(metaclass=ABCMeta):
    """Abstract VMCP assistant."""

    _configuration: dict
    """dict: Configuration."""

    _channel: Sender
    """Sender: OSC sender channel instance."""

    @property
    def is_open(self) -> bool:
        """bool: Returns whether the assistant is currently started."""
        return self._opened

    def __init__(
        self: AssistantClass,
        configuration: dict,
        osc: OSC
    ) -> None:
        """Instantiate.

        Args:
            configuration (dict):
                Configuration.
            osc (OSC):
                OSC instance.

        """
        self._opened = False
        self._configuration = configuration
        self._configuration["started-at"] = float(Timestamp(
            self._configuration.get("started-at", None)
        ))
        self._channel = osc.create_sender(
            self._configuration["host"],
            self._configuration["port"],
            self._configuration["channel"]
        )

    @abstractmethod
    def open(self: AssistantClass) -> AssistantClass:
        """Start.

        Returns:
            AssistantClass:
                Instance.

        """

    def __enter__(self: AssistantClass) -> AssistantClass:
        """Return the object instance for usage of the ``with`` statement.

        Returns:
            AssistantClass:
                Instance.

        """
        self.open()
        return self

    @abstractmethod
    def process(
        self: AssistantClass,
        data: Any
    ) -> None:
        """Process given input.

        Args:
            data (Any):
                Input.

        """

    @abstractmethod
    def close(self: AssistantClass) -> None:
        """Terminate."""

    def __exit__(
        self: AssistantClass,
        exc_type: type[BaseException] = None,
        exc_value: BaseException = None,
        traceback: TracebackType = None
    ) -> Literal[False]:
        """Return everytime ``False`` if exiting the runtime context.

        Args:
            exc_type (Optional[type[BaseException]]):
                Exception type.
            exc_value (Optional[BaseException]):
                Exception instance.
            traceback (Optional[TracebackType]):
                Exception context.

        Returns:
            bool:
                Everytime ``False``.

        """
        self.close()
        return False
